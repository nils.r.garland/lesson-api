const express = require("express");
const app = express();
const port = 3003;

app.get('/19', (req, res) => {
    res.send({
        "status": 200,
        "message": "Welcome to the Lesson 19 API!"
    });
});

app.get('/19/categories', (req, res) => {
    res.send({
        "status": 200,
        "categories": [
            {
                "id": 1,
                "name": "HTML",
                "type": "Markup Language",
                "description": "Markup language for structuring web content.",
                "color": ["pink", 500]
            },
            {
                "id": 2,
                "name": "CSS",
                "type": "Stylesheet",
                "description": "Stylesheets for designing web visuals and layouts.",
                "color": ["blue", 500]
            },
            {
                "id": 3,
                "name": "JavaScript",
                "type": "Programming Language",
                "description": "Programming language for web interactivity.",
                "color": ["orange", 500]
            },
            {
                "id": 4,
                "name": "React",
                "type": "UI Library",
                "description": "JavaScript library for building UI components.",
                "color": ["blue", 800]
            },
            {
                "id": 5,
                "name": "Express",
                "type": "Web server",
                "description": "Fast, unopinionated web server framework for Node.js.",
                "color": ["red", 500]
            }
        ]
    });
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});